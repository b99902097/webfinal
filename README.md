# Painter

## Introduction

https://myjian.github.io/Web-Project/

## Before running it locally

    npm install -g firebase-tools

## Run it locally

    firebase emulators:start

## Deploy to Firebase

    firebase deploy

## Members

National Taiwan University
Department of Computer Science and Information Engineering

- B99902067 Ming-Yuan Jian
- B99902107 Chia-Rui Lin
- B99902097 Yi Chien
- B99501062 Sheng-Lun Wei
