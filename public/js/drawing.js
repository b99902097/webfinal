var flag = "marker";
var c = document.getElementById("Canvas");
var ctx = document.getElementById("Canvas").getContext("2d");
var up_c = document.getElementById("upCanvas");
var up_ctx = document.getElementById("upCanvas").getContext("2d");
var drawMode = false;
var x, y, endX, endY;
var outOfCanvas = false;
var oriR, oriG, oriB;
var _color;
var offsetTop = 51;
var offsetLeft = 1;
var colorLayer;

$("#drawing").on("mouseout", function (event) {
  outOfCanvas = true;
  if (flag === "eraser") {
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
  }
});

$("body").on("mouseup", function (event) {
  if (outOfCanvas) {
    endDrawing(event);
  }
});

$(document).keyup(function (e) {
  if (e.keyCode == 27) {
    // esc
    drawMode = false;
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
  }
});

$("#drawing").on("mousedown", function (event) {
  outOfCanvas = false;
  x = endX = event.pageX - offsetLeft;
  y = endY = event.pageY - offsetTop;
  switch (flag) {
    case "line":
      drawMode = true;
      break;
    case "marker":
      drawMode = true;
      break;
    case "circle":
      drawMode = true;
      break;
    case "rectangle":
      drawMode = true;
      break;
    case "eraser":
      drawMode = true;
      startEraser(event);
      break;
    case "ellipse":
      drawMode = true;
      break;
    case "fill":
      drawMode = true;
      getRGBA(event);
      break;
    case "text":
      prepareText(event);
      break;
    default:
      alert("nothing");
      break;
  }
});

function startEraser(event) {
  x = endX = event.pageX - offsetLeft;
  y = endY = event.pageY - offsetTop;
  sendEraser(x, y, endX, endY);
}

function getRGBA(event) {
  x = event.pageX - offsetLeft;
  y = event.pageY - offsetTop;
  var oriData = ctx.getImageData(x, y, 1, 1).data;
  oriR = oriData[0];
  oriG = oriData[1];
  oriB = oriData[2];
  oriA = oriData[3];
}

function matchStartColor(pixelPos) {
  var r = colorLayer.data[pixelPos];
  var g = colorLayer.data[pixelPos + 1];
  var b = colorLayer.data[pixelPos + 2];
  return r == oriR && g == oriG && b == oriB;
}

function toTen(str) {
  var value = 0;
  switch (str[0]) {
    case "a":
      value += 16 * 10;
      break;
    case "b":
      value += 16 * 11;
      break;
    case "c":
      value += 16 * 12;
      break;
    case "d":
      value += 16 * 13;
      break;
    case "e":
      value += 16 * 14;
      break;
    case "f":
      value += 16 * 15;
      break;
    default:
      value += 16 * parseInt(str[0]);
      break;
  }

  switch (str[1]) {
    case "a":
      value += 10;
      break;
    case "b":
      value += 11;
      break;
    case "c":
      value += 12;
      break;
    case "d":
      value += 13;
      break;
    case "e":
      value += 14;
      break;
    case "f":
      value += 15;
      break;
    default:
      value += parseInt(str[1]);
      break;
  }
  return value;
}

function colorPixel(pixelPos) {
  colorLayer.data[pixelPos] = toTen(ctx.strokeStyle.substring(1, 3));
  colorLayer.data[pixelPos + 1] = toTen(ctx.strokeStyle.substring(3, 5));
  colorLayer.data[pixelPos + 2] = toTen(ctx.strokeStyle.substring(5, 7));
  colorLayer.data[pixelPos + 3] = 255;
}

function fillArea(px, py, r, g, b) {
  var origR = oriR;
  oriR = r;
  var origG = oriG;
  oriG = g;
  var origB = oriB;
  oriB = b;

  colorLayer = ctx.getImageData(0, 0, c.width, c.height);

  if (px <= 0 || py <= 0 || px >= c.width || py >= c.height) return;

  pixelStack = [[px, py]];
  while (pixelStack.length) {
    var newPos, x, y, pixelPos, reachLeft, reachRight;
    newPos = pixelStack.pop();
    x = newPos[0];
    y = newPos[1];

    pixelPos = (y * c.width + x) * 4;
    while (y-- >= 0 && matchStartColor(pixelPos)) {
      pixelPos -= c.width * 4;
    }

    pixelPos += c.width * 4;
    ++y;
    reachLeft = false;
    reachRight = false;
    while (y++ < c.height - 1 && matchStartColor(pixelPos)) {
      colorPixel(pixelPos);
      if (x > 0) {
        if (matchStartColor(pixelPos - 4)) {
          if (!reachLeft) {
            pixelStack.push([x - 1, y]);
            reachLeft = true;
          }
        } else if (reachLeft) {
          reachLeft = false;
        }
      }

      if (x < c.width - 1) {
        if (matchStartColor(pixelPos + 4)) {
          if (!reachRight) {
            pixelStack.push([x + 1, y]);
            reachRight = true;
          }
        } else if (reachRight) {
          reachRight = false;
        }
      }

      pixelPos += c.width * 4;
    }
  }
  ctx.putImageData(colorLayer, 0, 0);

  oriR = origR;
  oriG = origG;
  oriB = origB;

  /*
    var pixel = ctx.getImageData(px, py, 1, 1);
    var pixelData = pixel.data;
    if (pixelData[0] != oriR || pixelData[1] != oriG || pixelData[2] != oriB || pixelData[3] != oriA)
        return;
    else {
        ctx.fillStyle = ctx.strokeStyle;
        ctx.fillRect(px, py, 1, 1);

        fillArea(px+1, py);
        fillArea(px-1, py);
        fillArea(px, py+1);
        fillArea(px, py-1);
    }
    */
}

function prepareText(event) {
  if (event.target === document.getElementById("textPreview")) {
    return;
  }
  x = event.pageX - offsetLeft;
  y = event.pageY - offsetTop;

  console.log(x, y);
  var input = $("#textPreview");
  if (input.length === 0) {
    input = $('<input id="textPreview" type="text">');
    input.css("position", "absolute").css("margin", 0);
    input.on("keypress", function (e) {
      if (e.which === 13) {
        var input = $(this);
        var div = $(this).parents("div");
        if (input.val() !== "") {
          var x = this.offsetLeft + 2;
          var y = this.offsetTop - offsetTop + 2;
          sendText(x, y, input.val());
          console.log(x, y);
        }
        input.remove();
      }
    });
    input.on("keyup", function (e) {
      if (e.keyCode == 27) {
        // esc
        $(this).remove();
      }
    });
    $("#drawing").append(input);
  }
  input.css("font-size", ctx.lineWidth * 2).css("font-family", "sans-serif");
  input.css("left", x).css("top", y + offsetTop);
  input.css("color", ctx.strokeStyle);
}

$("#drawing").on("mousemove", function (event) {
  outOfCanvas = false;
  if (flag === "eraser") {
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
    endX = event.pageX - offsetLeft;
    endY = event.pageY - offsetTop;
    up_ctx.strokeRect(
      endX - ctx.lineWidth / 2,
      endY - ctx.lineWidth / 2,
      ctx.lineWidth,
      ctx.lineWidth
    );
  }
  if (!drawMode) return;

  if (flag === "line") {
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
    endX = event.pageX - offsetLeft;
    endY = event.pageY - offsetTop;
    up_ctx.beginPath();
    up_ctx.moveTo(x, y);
    up_ctx.lineTo(endX, endY);
    up_ctx.stroke();
  } else if (flag === "marker") {
    var nextX = event.pageX - offsetLeft;
    var nextY = event.pageY - offsetTop;
    sendCurve(endX, endY, nextX, nextY);
    endX = nextX;
    endY = nextY;
  } else if (flag === "circle") {
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
    endX = event.pageX - offsetLeft;
    endY = event.pageY - offsetTop;
    var dist = (x - endX) * (x - endX) + (y - endY) * (y - endY);
    up_ctx.beginPath();
    up_ctx.arc(
      (x + endX) / 2,
      (y + endY) / 2,
      Math.sqrt(dist) / 2,
      0,
      2 * Math.PI
    );
    up_ctx.stroke();
  } else if (flag === "rectangle") {
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
    endX = event.pageX - offsetLeft;
    endY = event.pageY - offsetTop;
    up_ctx.strokeRect(x, y, endX - x, endY - y);
  } else if (flag === "ellipse") {
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
    endX = event.pageX - offsetLeft;
    endY = event.pageY - offsetTop;

    var left, right, top, bottom;
    if (endX > x) {
      left = x;
      right = endX;
    } else {
      left = endX;
      right = x;
    }
    if (endY > y) {
      top = y;
      bottom = endY;
    } else {
      top = endY;
      bottom = y;
    }
    var height_2 = (bottom - top) / 2;
    var p1 = { x: left, y: bottom + height_2 / 3 };
    var p2 = { x: right, y: bottom + height_2 / 3 };
    var p3 = { x: right, y: top + height_2 };
    var p4 = { x: left, y: top - height_2 / 3 };
    var p5 = { x: right, y: top - height_2 / 3 };

    up_ctx.beginPath();
    up_ctx.moveTo(left, top + height_2);
    up_ctx.bezierCurveTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
    up_ctx.moveTo(left, top + height_2);
    up_ctx.bezierCurveTo(p4.x, p4.y, p5.x, p5.y, p3.x, p3.y);
    up_ctx.stroke();
  } else if (flag === "eraser") {
    sendEraser(x, y, endX, endY);
    x = endX;
    y = endY;
  }
});

$("#drawing").on("mouseup", function (event) {
  $("#textPreview").focus();
  endDrawing(event);
});

function endDrawing(event) {
  if (!drawMode) return;
  if (flag === "line") {
    sendStraightLine(x, y, endX, endY);
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
  } else if (flag === "circle") {
    dist = (x - endX) * (x - endX) + (y - endY) * (y - endY);
    sendCircle((x + endX) / 2, (y + endY) / 2, Math.sqrt(dist) / 2);
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
  } else if (flag === "rectangle") {
    sendRectangle(x, y, endX, endY);
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
  } else if (flag == "fill") {
    sendFill(x, y);
    //fillArea(x, y);
  } else if (flag === "ellipse") {
    var left, right, top, bottom;
    if (endX > x) {
      left = x;
      right = endX;
    } else {
      left = endX;
      right = x;
    }
    if (endY > y) {
      top = y;
      bottom = endY;
    } else {
      top = endY;
      bottom = y;
    }
    sendEllipse(left, top, right, bottom);
    up_ctx.clearRect(0, 0, up_c.width, up_c.height);
  } else if (flag === "eraser") {
    sendEraser(endX, endY, endX, endY);
  }
  drawMode = false;
}

function printText(x, y, text, width, color) {
  var origWidth = ctx.lineWidth;
  ctx.lineWidth = width;
  var origColor = ctx.strokeStyle;
  ctx.strokeStyle = color;

  ctx.font = ctx.lineWidth * 2 + "px sans-serif";
  //var gradient = ctx.createLinearGradient(0, 0, c.width, 0);
  //gradient.addColorStop("0.5", ctx.strokeStyle);
  //gradient.addColorStop("1.0", "red");
  //ctx.fillStyle = gradient;
  ctx.fillStyle = ctx.strokeStyle;
  ctx.textBaseline = "top";
  ctx.fillText(text, x, y + ctx.lineWidth / 2);

  ctx.lineWidth = origWidth;
  ctx.strokeStyle = origColor;
}

function drawCurve(x, y, endX, endY, width, color) {
  var origWidth = ctx.lineWidth;
  ctx.lineWidth = width;
  var origColor = ctx.strokeStyle;
  ctx.strokeStyle = color;

  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(endX, endY);
  ctx.stroke();

  ctx.lineWidth = origWidth;
  ctx.strokeStyle = origColor;
}

function drawEraser(x, y, endX, endY, width, color) {
  var origWidth = ctx.lineWidth;
  ctx.lineWidth = width;
  var origColor = ctx.strokeStyle;
  ctx.strokeStyle = color;

  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(endX, endY);
  ctx.stroke();
  ctx.fillStyle = "#FFFFFF";
  ctx.fillRect(
    endX - ctx.lineWidth / 2,
    endY - ctx.lineWidth / 2,
    ctx.lineWidth,
    ctx.lineWidth
  );

  ctx.lineWidth = origWidth;
  ctx.strokeStyle = origColor;
}

function drawStraightLine(x, y, endX, endY, width, color) {
  var origWidth = ctx.lineWidth;
  ctx.lineWidth = width;
  var origColor = ctx.strokeStyle;
  ctx.strokeStyle = color;

  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(endX, endY);
  ctx.stroke();

  ctx.lineWidth = origWidth;
  ctx.strokeStyle = origColor;
}

function drawRectangle(x, y, endX, endY, width, color) {
  var origWidth = ctx.lineWidth;
  ctx.lineWidth = width;
  var origColor = ctx.strokeStyle;
  ctx.strokeStyle = color;

  ctx.strokeRect(x, y, endX - x, endY - y);

  ctx.lineWidth = origWidth;
  ctx.strokeStyle = origColor;
}

function drawCircle(x, y, dist, width, color) {
  var origWidth = ctx.lineWidth;
  ctx.lineWidth = width;
  var origColor = ctx.strokeStyle;
  ctx.strokeStyle = color;

  ctx.beginPath();
  ctx.arc(x, y, dist, 0, 2 * Math.PI);
  ctx.stroke();

  ctx.lineWidth = origWidth;
  ctx.strokeStyle = origColor;
}

function drawEllipse(left, top, right, bottom, width, color) {
  var origWidth = ctx.lineWidth;
  ctx.lineWidth = width;
  var origColor = ctx.strokeStyle;
  ctx.strokeStyle = color;

  //       left      right
  //        p4        p5
  //    top |---------|
  //        |         |
  //        |---------| p3
  //        |         |
  // bottom |---------|
  //        p1        p2

  var height_2 = (bottom - top) / 2;
  var p1 = { x: left, y: bottom + height_2 / 3 };
  var p2 = { x: right, y: bottom + height_2 / 3 };
  var p3 = { x: right, y: top + height_2 };
  var p4 = { x: left, y: top - height_2 / 3 };
  var p5 = { x: right, y: top - height_2 / 3 };

  ctx.beginPath();
  ctx.moveTo(left, top + height_2);
  ctx.bezierCurveTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
  ctx.moveTo(left, top + height_2);
  ctx.bezierCurveTo(p4.x, p4.y, p5.x, p5.y, p3.x, p3.y);
  ctx.stroke();

  ctx.lineWidth = origWidth;
  ctx.strokeStyle = origColor;
}

// 顏色
function rgbToSimpleColor(rgbStr) {
  var rgbNum = rgbStr.split("(")[1].split(")")[0].split(", ");
  var rgb = (rgbNum[0] << 16) | (rgbNum[1] << 8) | rgbNum[2];
  return "#" + (0x1000000 | rgb).toString(16).substring(1).toUpperCase();
}

function setColor(event, value) {
  $(".selected").removeClass("selected");
  var newColor;
  if (value === "div") {
    newColor = $(event.target).addClass("selected").css("background-color");
    $("input[name=fgColor]").attr("value", rgbToSimpleColor(newColor));
  } else {
    newColor = value;
  }
  _color = newColor;
  $("#width").css("background-color", newColor);
  $("#textPreview").css("color", newColor);
  if (flag !== "eraser") {
    ctx.strokeStyle = newColor;
    up_ctx.strokeStyle = newColor;
  }
}

// 筆粗
function setWidth(value) {
  ctx.lineWidth = value;
  if (flag !== "eraser") up_ctx.lineWidth = value;
  else ctx.lineWidth *= 2;
  document.getElementById("lineWidth").value = value;
  $("#width").css("height", value + "px");
  $("#textPreview").css("font-size", value * 2);
}

// 清除畫面
$("#clear").click(function (event) {
  ctx.fillStyle = "#FFFFFF";
  ctx.fillRect(0, 0, c.width, c.height);
  myDataRef.remove();
});

// 儲存畫面
$("#export").click(function (event) {
  const imgUrl = c.toDataURL("image/png");
  const linkElem = event.target;
  linkElem.download = canvasId + ".png";
  linkElem.href = imgUrl;
});

function switchTool(event) {
  if (event.target.id === flag) return;
  if (flag === "eraser") {
    // disable eraser
    ctx.lineWidth = ctx.lineWidth / 2;
    ctx.strokeStyle = _color;
    up_ctx.lineWidth = ctx.lineWidth;
    up_ctx.strokeStyle = _color;
    $("canvas").css("cursor", "auto");
  } else if (flag === "text") {
    // disable text
    $("#textPreview").remove();
  }

  flag = event.target.id;
  if (flag === "eraser") {
    // enable eraser
    ctx.lineWidth = ctx.lineWidth * 2;
    ctx.strokeStyle = "#FFFFFF";
    up_ctx.lineWidth = 1;
    up_ctx.strokeStyle = "#000000";
    $("canvas").css("cursor", "none");
  }
}

ctx.fillStyle = "#FFFFFF";
ctx.fillRect(0, 0, c.width, c.height);
up_ctx.lineCap = "round";
up_ctx.lineJoin = "round";
ctx.lineCap = "round";
ctx.lineJoin = "round";
setWidth($("#widthSlider").val());
setColor(null, "#000000");
$("#" + flag).button("toggle");

// Firebase
const queryParams = new URLSearchParams(window.location.search);
const canvasId = queryParams.get("canvasId") || "undefined";

var firebaseUrl = "https://simple-painter.firebaseio.com/" + canvasId;
var myDataRef = new Firebase(firebaseUrl + "/draw");
myDataRef.on("child_added", function (snapshot) {
  var obj = snapshot.val();
  switch (obj.tool) {
    case "rectangle":
      drawRectangle(
        obj.p1.x,
        obj.p1.y,
        obj.p2.x,
        obj.p2.y,
        obj.width,
        obj.color
      );
      break;
    case "circle":
      drawCircle(obj.p1.x, obj.p1.y, obj.dist, obj.width, obj.color);
      break;
    case "line":
      drawStraightLine(
        obj.p1.x,
        obj.p1.y,
        obj.p2.x,
        obj.p2.y,
        obj.width,
        obj.color
      );
      break;
    case "marker":
      drawCurve(obj.p1.x, obj.p1.y, obj.p2.x, obj.p2.y, obj.width, obj.color);
      break;
    case "ellipse":
      drawEllipse(obj.p1.x, obj.p1.y, obj.p2.x, obj.p2.y, obj.width, obj.color);
      break;
    case "text":
      printText(obj.p1.x, obj.p1.y, obj.string, obj.width, obj.color);
      break;
    case "fill":
      fillArea(obj.p1.x, obj.p1.y, obj.R, obj.G, obj.B);
      break;
    case "eraser":
      drawEraser(obj.p1.x, obj.p1.y, obj.p2.x, obj.p2.y, obj.width, obj.color);
      break;
    default:
      console.log(obj);
      console.log('not supported tool type "' + obj.tool + '"');
      break;
  }
});

myDataRef.on("value", function (snapshot) {
  var obj = snapshot.val();
  if (obj === null) {
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(0, 0, c.width, c.height);
  }
});

function sendFill(x, y) {
  myDataRef.push({
    tool: "fill",
    p1: { x: x, y: y },
    R: oriR,
    G: oriG,
    B: oriB,
  });
}

function sendText(x, y, text) {
  myDataRef.push({
    tool: "text",
    p1: { x: x, y: y },
    string: text,
    color: _color,
    width: ctx.lineWidth,
  });
}

function sendCurve(x, y, endX, endY) {
  myDataRef.push({
    tool: "marker",
    p1: { x: x, y: y },
    p2: { x: endX, y: endY },
    color: _color,
    width: ctx.lineWidth,
  });
}

function sendStraightLine(x, y, endX, endY) {
  myDataRef.push({
    tool: "line",
    p1: { x: x, y: y },
    p2: { x: endX, y: endY },
    color: _color,
    width: ctx.lineWidth,
  });
}

function sendRectangle(x, y, endX, endY) {
  myDataRef.push({
    tool: "rectangle",
    p1: { x: x, y: y },
    p2: { x: endX, y: endY },
    color: _color,
    width: ctx.lineWidth,
  });
}

function sendCircle(x, y, dist) {
  myDataRef.push({
    tool: "circle",
    p1: { x: x, y: y },
    dist: dist,
    color: _color,
    width: ctx.lineWidth,
  });
}

function sendEllipse(left, top, right, bottom) {
  myDataRef.push({
    tool: "ellipse",
    p1: { x: left, y: top },
    p2: { x: right, y: bottom },
    color: _color,
    width: ctx.lineWidth,
  });
}

function sendEraser(x, y, endX, endY) {
  myDataRef.push({
    tool: "eraser",
    p1: { x: x, y: y },
    p2: { x: endX, y: endY },
    color: ctx.strokeStyle,
    width: ctx.lineWidth,
  });
}

var myChatRef = new Firebase(firebaseUrl + "/chat");
var username;
myChatRef.on("child_added", function (snapshot) {
  var obj = snapshot.val();
  var li = $("<li></li>")
    .css("color", obj.color)
    .text(obj.name + ": " + obj.text)
    .attr("title", new Date(parseInt(obj.date)).toString());
  li.appendTo("#chat");
  if (li.index() >= 100) {
    $("#chat > li")[0].remove();
  }
  var ul = document.getElementById("chat");
  ul.scrollTop = ul.scrollHeight;
});

$("#message").on("keypress", function (event) {
  var msg = $(event.target).val();
  if (event.which === 13 && msg !== "") {
    if ($(event.target).attr("placeholder") !== "") {
      $(event.target).attr("placeholder", "");
      username = msg;
      myChatRef.push({
        name: "[System]",
        text: username + " has joined the chat.",
        color: "green",
        date: Date.now(),
      });
    } else {
      myChatRef.push({
        name: username,
        text: msg,
        color: _color,
        date: Date.now(),
      });
    }
    $(event.target).val("");
  }
});

window.addEventListener("beforeunload", () => {
  if (username) {
    myChatRef.push({
      name: "[System]",
      text: username + " has left the chat.",
      color: "red",
      date: Date.now(),
    });
  }
});
