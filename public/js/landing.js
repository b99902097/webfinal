(function () {
  function handleCanvasId(str) {
    str = "" + str;
    str = str.replace(/\./g, "-");
    str = str.replace(/#/g, "-");
    str = str.replace(/\$/g, "-");
    str = str.replace(/\[/g, "-");
    str = str.replace(/\]/g, "-");
    return str;
  }

  function handleNavigateToCanvas() {
    const newNameElem = document.getElementById("newName");
    const canvasId =
      newNameElem && newNameElem.value
        ? handleCanvasId(newNameElem.value)
        : Date.now();
    window.location.assign(
      "./?" + new URLSearchParams({ canvasId: canvasId }).toString()
    );
  }

  $("#createNew").click(handleNavigateToCanvas);

  // when Enter is pressed, e.which === 13
  $("#newName").on("keypress", function (e) {
    if (e.which === 13) {
      handleNavigateToCanvas();
    }
  });

  $("#headtext").ready(function (event) {
    var colours = [
      "#DC0300",
      "#FA8000",
      "#D9DC00",
      "#00DC03",
      "#006BDC",
      "#0300DC",
      "#8F19FF",
    ];

    // Retrieve the words
    var headtext = $("#headtext");
    if (headtext.length === 0) return;
    var text = headtext.html().split("");

    // Empty the elment
    $("#headtext").empty();

    // Iterate over the words
    $.each(text, function (i, word) {
      if (i != text.length) {
        word += ""; // Add space after word
      }

      // Get random color
      var colourIndex = Math.floor(Math.random() * colours.length);

      $("<span></span>")
        .html(word)
        .css("color", colours[colourIndex])
        .appendTo($("#headtext"));
    });
  });
})();
