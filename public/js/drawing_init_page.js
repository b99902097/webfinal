(function () {
  const queryParams = new URLSearchParams(window.location.search);
  const canvasId = queryParams.get("canvasId") || "undefined";

  document.title = "Painter - " + canvasId;

  const chatboxTitle = document.getElementById("chatboxTitle");
  chatboxTitle.innerHTML = canvasId + " 的聊天室";

  const fbShare = document.getElementById("share");
  fbShare.dataset.href =
    window.location.origin +
    window.location.pathname +
    "?" +
    new URLSearchParams({ canvasId: canvasId }).toString();
})();
